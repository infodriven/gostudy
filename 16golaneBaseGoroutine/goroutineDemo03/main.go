package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

func main() {
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go hello(i)
	}
	wg.Wait()
}

//多次执行上面代码，挥发新每次打印的数字顺序都不一样

func hello(i int) {
	defer wg.Done() //goroutine结束就登记-1
	fmt.Println("Hello Goroutine!", i)
}
