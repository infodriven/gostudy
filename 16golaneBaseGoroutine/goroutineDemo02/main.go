package main

import (
	"fmt"
	"time"
)

func main() {
	go hello() //启动另外一个goroutine去执行hello函数
	fmt.Println("main goroutine done!")
	time.Sleep(time.Second)
}

/*
执行上面的代码你会发现，这一次先打印main goroutine done!，然后紧接着打印Hello Goroutine!
首先为什么会先打印main goroutine done!是因为我们在创建新的goroutine的时候需要花费一些时间，
而此时main函数所在的goroutine是继续执行的。
*/

func hello() {
	fmt.Println("Hello Goroutine!")
}
