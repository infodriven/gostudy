package main

import "fmt"

// 启动单个goroutine
func main() {
	hello()
	fmt.Println("Helo goroutine done")
}

//这个示例hello函数和下面的语句是串行的，执行结果是打印完Hello Goroutine!后打印main goroutine done!

func hello() {
	fmt.Println("Hello Goroutine!")
}
