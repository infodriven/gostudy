package main

import "fmt"

func main() {
	createSlice01()
	createSlice02()
	createSlice03()
	createSlice04()
	createSlice05()
	createSlice06()
	createSlice07()
	createSlice08()
	createSlice09()
	createSlice10()
	createSlice11()
}

func createSlice01() {
	//声明切片类型
	var a []string                           //声明一个字符串切片
	var b = []int{1, 2, 3, 4, 5, 6, 7, 8, 9} //声明一个整型切片并初始化
	var c = []bool{false, true}              //声明一个布尔切片并初始化
	var d = []bool{false, true}              //声明一个布尔切片并初始化
	fmt.Println(a)                           //[]
	fmt.Println(b)                           //[]
	fmt.Println(c)
	fmt.Println(d)        //[false true]
	fmt.Println(a == nil) //true
	fmt.Println(b == nil) //false
	fmt.Println(c == nil) //false
	// fmt.Println(c == d)   //切片是引用类型，不支持直接比较，只能和nil比较
}

func createSlice02() {
	//简单切片表达式
	a := [5]int{1, 2, 3, 4, 5}
	s := a[1:3] // s := a[low:high]
	fmt.Printf("s:%v len(s):%v cap(s):%v\n", s, len(s), cap(s))
	//输出：s:[2 3] len(s):2 cap(s):4
}

func createSlice03() {
	//切片表达式
	a := [5]int{1, 2, 3, 4, 5}
	s := a[1:3] // s := a[low:high]
	fmt.Printf("s:%v len(s):%v cap(s):%v\n", s, len(s), cap(s))
	s2 := s[3:4] // 索引的上限是cap(s)而不是len(s)
	fmt.Printf("s2:%v len(s2):%v cap(s2):%v\n", s2, len(s2), cap(s2))
	/*
		输出：
		s:[2 3] len(s):2 cap(s):4
		s2:[5] len(s2):1 cap(s2):1
	*/
}

func createSlice04() {
	//完整切片表达式
	a := [5]int{1, 2, 3, 4, 5}
	t := a[1:3:5]
	fmt.Printf("t:%v len(t):%v cap(t):%v\n", t, len(t), cap(t))
	//输出：t:[2 3] len(t):2 cap(t):4
}

func createSlice05() {
	//使用make()函数构造切片
	a := make([]int, 2, 10)
	fmt.Println(a)      //[0 0]
	fmt.Println(len(a)) //2
	fmt.Println(cap(a)) //10
}

func createSlice06() {
	//切片赋值拷贝
	s1 := make([]int, 3) //[0 0 0]
	s2 := s1             //将s1直接赋值给s2，s1和s2共用一个底层数组
	s2[0] = 100
	fmt.Println(s1) //[100 0 0]
	fmt.Println(s2) //[100 0 0]
}

func createSlice07() {
	//切片遍历
	s := []int{1, 3, 5}
	for i := 0; i < len(s); i++ {
		fmt.Println(i, s[i])
	}
	for index, value := range s {
		fmt.Println(index, value)
	}
}

func createSlice08() {
	//append()方法为切片添加元素
	var s []int
	s = append(s, 1)       // [1]
	s = append(s, 2, 3, 4) // [1 2 3 4]
	s2 := []int{5, 6, 7}
	s = append(s, s2...) // [1 2 3 4 5 6 7]
}

func createSlice09() {
	//append()添加元素和切片扩容
	var numSlice []int
	for i := 0; i < 10; i++ {
		numSlice = append(numSlice, i)
		fmt.Printf("%v  len:%d  cap:%d  ptr:%p\n", numSlice, len(numSlice), cap(numSlice), numSlice)
	}
}

func createSlice10() {
	a := []int{1, 2, 3, 4, 5}
	b := a
	fmt.Println(a) //[1 2 3 4 5]
	fmt.Println(b) //[1 2 3 4 5]
	b[0] = 1000
	fmt.Println(a) //[1000 2 3 4 5]
	fmt.Println(b) //[1000 2 3 4 5]

	// copy()复制切片
	c := []int{1, 2, 3, 4, 5}
	d := make([]int, 5, 5)
	copy(d, c)     //使用copy()函数将切片a中的元素复制到切片c
	fmt.Println(c) //[1 2 3 4 5]
	fmt.Println(d) //[1 2 3 4 5]
	d[0] = 1000
	fmt.Println(c) //[1 2 3 4 5]
	fmt.Println(d) //[1000 2 3 4 5]
}

func createSlice11() {
	// 从切片中删除元素
	a := []int{30, 31, 32, 33, 34, 35, 36, 37}
	// 要删除索引为2的元素
	a = append(a[:2], a[3:]...)
	fmt.Println(a) //[30 31 33 34 35 36 37]
}
