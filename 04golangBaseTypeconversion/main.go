package main

import (
	"fmt"
	"strconv"
)

func main() {
	createAtoi()
	createItoa()
	createParseBool()
	createParseInt()
	createParseUint()
	createParseFloat()
	createFormatInt()
	createFormatUint()
	createFormatBool()
	createFormatFloat()
	createAppend()
}

// 字符串类型转换Int类型
func createAtoi() {
	s1 := "100"
	i1, err := strconv.Atoi(s1)
	if err != nil {
		fmt.Println("can't convert to int")
	} else {
		fmt.Printf("type:%T value:%#v\n", i1, i1) //type:int value:100
	}
}

// Int类型转换字符串类型
func createItoa() {
	i2 := 200
	s2 := strconv.Itoa(i2)
	fmt.Printf("type:%T value:%#v\n", s2, s2) //type:string value:"200"
}

// 字符串类型转Bool类型
func createParseBool() {
	str1 := "110"
	boo1, err := strconv.ParseBool(str1)
	if err != nil {
		fmt.Printf("str1: %v\n", err)
	} else {
		fmt.Println(boo1)
	}
	str2 := "t"
	boo2, err := strconv.ParseBool(str2)
	if err != nil {
		fmt.Printf("str2: %v\n", err)
	} else {
		fmt.Println(boo2)
	}
}

// 字符串类型转Int类型
func createParseInt() {
	str := "-11"
	num, err := strconv.ParseInt(str, 10, 0)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(num)
	}
}

// 字符串类型转uint类型
func createParseUint() {
	str := "11"
	num, err := strconv.ParseUint(str, 10, 0)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(num)
	}
}

// 字符串类型转float类型
func createParseFloat() {
	str := "3.1415926"
	num, err := strconv.ParseFloat(str, 64)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(num)
	}
}

// Bool转字符串
func createFormatBool() {
	num := true
	str := strconv.FormatBool(num)
	fmt.Printf("type:%T,value:%v\n ", str, str)
}

// Int转字符串
func createFormatInt() {
	var num int64 = 100
	str := strconv.FormatInt(num, 16)
	fmt.Printf("type:%T,value:%v\n ", str, str)
}

// Uint转字符串
func createFormatUint() {
	var num uint64 = 100
	str := strconv.FormatUint(num, 16)
	fmt.Printf("type:%T,value:%v\n ", str, str)
}

// Float转字符串
func createFormatFloat() {
	var num float64 = 3.1415926
	str := strconv.FormatFloat(num, 'E', -1, 64)
	fmt.Printf("type:%T,value:%v\n ", str, str)
}

// Int数组添加
func createAppend() {
	// 声明一个slice
	b10 := []byte("int (base 10):")

	// 将转换为10进制的string，追加到slice中
	b10 = strconv.AppendInt(b10, -42, 10)
	fmt.Println(string(b10))
	b16 := []byte("int (base 16):")
	b16 = strconv.AppendInt(b16, -42, 16)
	fmt.Println(string(b16))
}
