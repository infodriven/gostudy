package main

import "fmt"

func main() {
	createPoint01()
	createPoint02()
	createPoint03()
	createPoint04()

	//指针取值示例
	a := 10
	modify1(a)
	fmt.Println(a) // 10
	modify2(&a)
	fmt.Println(a) // 100

}

// 指针地址和指针类型
func createPoint01() {
	a := 10
	b := &a
	fmt.Printf("a:%d ptr:%p\n", a, &a) // a:10 ptr:0xc00001a078
	fmt.Printf("b:%p type:%T\n", b, b) // b:0xc00001a078 type:*int
	fmt.Println(&b)                    // 0xc00000e018
}

// 取指针
func createPoint02() {
	//指针取值
	a := 10
	b := &a                          // 取变量a的地址，将指针保存到b中
	fmt.Printf("type of b:%T\n", b)  // type of b:*int
	c := *b                          // 指针取值（根据指针去内存取值）
	fmt.Printf("type of c:%T\n", c)  //type of c:int
	fmt.Printf("value of c:%v\n", c) //value of c:10
}

// 指针取值示例
func modify1(x int) {
	x = 100
}
func modify2(x *int) {
	*x = 100
}

// new函数分配类型指针
func createPoint03() {
	a := new(int)
	b := new(bool)
	fmt.Printf("%T\n", a) // *int
	fmt.Printf("%T\n", b) // *bool
	fmt.Println(*a)       // 0
	fmt.Println(*b)       // false
}

// 指针类型初始化
func createPoint04() {
	var a *int
	a = new(int)
	*a = 10
	fmt.Println(*a)
}
