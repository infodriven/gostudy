package main

import (
	"fmt"
	"reflect"
)

// 自定义类型
type myInt int64

// 定义结构体
type person struct {
	name string
	age  int
}
type book struct{ title string }

func main() {
	//通过反射获取任意类型
	var a float32 = 3.14
	reflectType(a) // type:float32
	var b int64 = 100
	reflectType(b) // type:int64
	var c myInt
	reflectType(c)
	var d = person{
		name: "沙河小王子",
		age:  18,
	}
	reflectType(d)
	var e = book{title: "《跟小王子学Go语言》"}
	reflectType(e)

	//通过反射获取值
	reflectValue(a)
	reflectValue(b)
	reflectValue(c)
	reflectValue(d)
}

func reflectType(x interface{}) {
	v := reflect.TypeOf(x)
	fmt.Printf("type:%v\n", v)
}

func reflectValue(x interface{}) {
	v := reflect.ValueOf(x)
	k := v.Kind()
	switch k {
	case reflect.Int64:
		// v.Int()从反射中获取整型的原始值，然后通过int64()强制类型转换
		fmt.Printf("type is int64, value is %d\n", int64(v.Int()))
	case reflect.Float32:
		// v.Float()从反射中获取浮点型的原始值，然后通过float32()强制类型转换
		fmt.Printf("type is float32, value is %f\n", float32(v.Float()))
	case reflect.Float64:
		// v.Float()从反射中获取浮点型的原始值，然后通过float64()强制类型转换
		fmt.Printf("type is float64, value is %f\n", float64(v.Float()))
	}
}
