package main

import (
	"fmt"
	"math"
)

func main() {
	createInt()
	createFloat()
	createComplex()
	createBool()
	createString()
	createByteRune()
	sqrtDemo()
	traversalString()
	changeString()
}

// 整型数据类型
func createInt() {
	// 十进制
	var a int = 10
	fmt.Printf("%d \n", a) // 10
	fmt.Printf("%b \n", a) // 1010  占位符%b表示二进制

	// 八进制  以0开头
	var b int = 077
	fmt.Printf("%o \n", b) // 77

	// 十六进制  以0x开头
	var c int = 0xff
	fmt.Printf("%x \n", c) // ff
	fmt.Printf("%X \n", c) // FF
}

// 浮点数类型
func createFloat() {
	fmt.Printf("%f\n", math.Pi)
	fmt.Printf("%.2f\n", math.Pi)
}

// 复数类型
func createComplex() {
	var c1 complex64
	c1 = 1 + 2i
	var c2 complex128
	c2 = 2 + 3i
	fmt.Println(c1)
	fmt.Println(c2)
}

// 布尔类型
func createBool() {
	var T1 bool = true
	var T2 bool = false
	fmt.Println(T1, T2)
}

// 字符串类型
func createString() {
	s1 := "hello"
	s2 := "你好"

	//打印Window路径
	fmt.Println("str := \"c:\\Code\\lesson1\\go.exe\"")

	//多行字符串
	s3 := `第一行
第二行
第三行
`
	fmt.Print(s3)
	fmt.Println(s1, s2)
}

// byte&rune
func createByteRune() {
	var a = '中'
	var b = 'x'
	fmt.Println(a, b)
}

// 遍历字符创
func traversalString() {
	s := "hello沙河"
	for i := 0; i < len(s); i++ { //byte
		fmt.Printf("%v(%c) ", s[i], s[i])
	}
	fmt.Println()
	for _, r := range s { //rune
		fmt.Printf("%v(%c) ", r, r)
	}
	fmt.Println()
}

/*
输出内容：
104(h) 101(e) 108(l) 108(l) 111(o) 230(æ) 178(²) 153() 230(æ) 178(²) 179(³)
104(h) 101(e) 108(l) 108(l) 111(o) 27801(沙) 27827(河)
*/

// 修改字符串
func changeString() {
	s1 := "big"
	// 强制类型转换
	byteS1 := []byte(s1)
	byteS1[0] = 'p'
	fmt.Println(string(byteS1))
	s2 := "白萝卜"
	runeS2 := []rune(s2)
	runeS2[0] = '红'
	fmt.Println(string(runeS2))
}

// 类型转换
func sqrtDemo() {
	var a, b = 3, 4
	var c int
	// math.Sqrt()接收的参数是float64类型，需要强制转换
	c = int(math.Sqrt(float64(a*a + b*b)))
	fmt.Println(c)
}
