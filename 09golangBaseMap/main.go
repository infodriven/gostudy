package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

func main() {
	createMap01()
	createMap02()
	createMap03()
	createMap04()
	createMap05()
	createMap06()
	createMap07()
	createMap08()
}

func createMap01() {
	//字典声明初始化
	// 第一种方法
	var scores01 map[string]int = map[string]int{"english": 80, "chinese": 85}

	// 第二种方法
	scores02 := map[string]int{"english": 80, "chinese": 85}

	// 第三种方法
	scores03 := make(map[string]int)
	scores03["english"] = 80
	scores03["chinese"] = 85
	fmt.Println(scores01)
	fmt.Println(scores02)
	fmt.Println(scores03)
}

func createMap02() {
	// 声明一个名为 score 的字典
	var scores map[string]int

	// 未初始化的 score 的零值为nil，无法直接进行赋值
	if scores == nil {
		// 需要使用 make 函数先对其初始化
		scores = make(map[string]int)
	}

	// 经过初始化后，就可以直接赋值
	scores["chinese"] = 90
	fmt.Println(scores)
}

func createMap03() {
	//判断某个键是否存在
	scoreMap := make(map[string]int)
	scoreMap["张三"] = 90
	scoreMap["小明"] = 100
	// 如果key存在ok为true,v为对应的值；不存在ok为false,v为值类型的零值
	v, ok := scoreMap["张三"]
	if ok {
		fmt.Println(v)
	} else {
		fmt.Println("查无此人")
	}
}

func createMap04() {
	scoreMap01 := make(map[string]int)
	scoreMap01["张三"] = 90
	scoreMap01["小明"] = 100
	scoreMap01["娜扎"] = 60
	for k, v := range scoreMap01 {
		fmt.Println(k, v)
	}

	//只想遍历key的时候，可以按下面的写法
	scoreMap02 := make(map[string]int)
	scoreMap02["张三"] = 90
	scoreMap02["小明"] = 100
	scoreMap02["娜扎"] = 60
	for k := range scoreMap02 {
		fmt.Println(k)
	}
}

func createMap05() {
	//使用delete()函数删除键值对
	scoreMap := make(map[string]int)
	scoreMap["张三"] = 90
	scoreMap["小明"] = 100
	scoreMap["娜扎"] = 60
	delete(scoreMap, "小明") //将小明:100从map中删除
	for k, v := range scoreMap {
		fmt.Println(k, v)
	}
}

func createMap06() {
	//按照顺序遍历map
	rand.Seed(time.Now().UnixNano()) //初始化随机数种子
	var scoreMap = make(map[string]int, 200)
	for i := 0; i < 100; i++ {
		key := fmt.Sprintf("stu%02d", i) //生成stu开头的字符串
		value := rand.Intn(100)          //生成0~99的随机整数
		scoreMap[key] = value
	}
	//取出map中的所有key存入切片keys
	var keys = make([]string, 0, 200)
	for key := range scoreMap {
		keys = append(keys, key)
	}
	//对切片进行排序
	sort.Strings(keys)
	//按照排序后的key遍历map
	for _, key := range keys {
		fmt.Println(key, scoreMap[key])
	}
}

func createMap07() {
	//元素为map类型的切片
	var mapSlice = make([]map[string]string, 3)
	for index, value := range mapSlice {
		fmt.Printf("index:%d value:%v\n", index, value)
	}
	fmt.Println("after init")
	// 对切片中的map元素进行初始化
	mapSlice[0] = make(map[string]string, 10)
	mapSlice[0]["name"] = "小王子"
	mapSlice[0]["password"] = "123456"
	mapSlice[0]["address"] = "沙河"
	for index, value := range mapSlice {
		fmt.Printf("index:%d value:%v\n", index, value)
	}
}

func createMap08() {
	//值为切片类型的map
	var sliceMap = make(map[string][]string, 3)
	fmt.Println(sliceMap)
	fmt.Println("after init")
	key := "中国"
	value, ok := sliceMap[key]
	if !ok {
		value = make([]string, 0, 2)
	}
	value = append(value, "北京", "上海")
	sliceMap[key] = value
	fmt.Println(sliceMap)
}
