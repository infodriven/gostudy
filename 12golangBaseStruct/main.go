package main

import (
	"encoding/json"
	"fmt"
)

// 全局person
type person struct {
	name string
	city string
	age  int
}

func main() {
	createTypeAlias()
	createStruct01()
	createStruct02()
	createStruct03()
	createStruct04()
	createStruct05()
	createStruct06()
	createStruct07()
	createStruct08()
	createStruct09()
	createStruct09()
	createStruct10()
	createStruct11()

	//调用构造函数
	p9 := newPerson("张三", "沙河", 90)
	fmt.Printf("%#v\n", p9) //&main.person{name:"张三", city:"沙河", age:90}
	p9.Dream()
	fmt.Println(p9.age)
	p9.SetAge(28)
	fmt.Println(p9.age)
	p9.SetAge2(20)
	fmt.Println(p9.age)
}

// 类型别名与自定义类型
func createTypeAlias() {
	//类型定义
	type NewInt int
	//类型别名
	type MyInt = int

	var a NewInt
	var b MyInt

	fmt.Printf("type of a:%T\n", a) // type kind of a:main.NewInt
	fmt.Printf("type of b:%T\n", b) //type kind of b:int
}

// 定义结构体
func createStruct01() {
	type person struct {
		name string
		city string
		age  int8
	}

	type person1 struct {
		name, city string
		age        int8
	}

	p := person{}
	p1 := person1{}

	fmt.Println(p)
	fmt.Println(p1)
}

// 结构体实例化
func createStruct02() {
	type person struct {
		name string
		city string
		age  int
	}

	var p2 person
	p2.name = "卡里"
	p2.city = "北京"
	p2.age = 28

	fmt.Printf("p1=%v\n", p2)  // p1={卡里 北京 28}
	fmt.Printf("p1=%#v\n", p2) // p1=main.person{name:"卡里", city:"北京", age:28}
}

// 匿名结构体
func createStruct03() {
	var user struct {
		Name string
		Age  int
	}
	user.Name = "卡里"
	user.Age = 28
	fmt.Printf("%#v\n", user)
}

// 指针类型结构体
func createStruct04() {
	type person struct {
		name string
		city string
		age  int
	}

	var p3 = new(person)
	fmt.Printf("%T\n", p3)     //*main.person
	fmt.Printf("p2=%#v\n", p3) //p2=&main.person{name:"", city:"", age:0}

	var p4 = new(person)
	p4.name = "卡里"
	p4.city = "北京"
	p4.age = 28
	fmt.Printf("p2=%#v\n", p4)
}

// 取结构体的地址实例化
func createStruct05() {
	type person struct {
		name string
		city string
		age  int
	}

	p5 := &person{}
	fmt.Printf("%T\n", p5)     //*main.person
	fmt.Printf("p3=%#v\n", p5) //p3=&main.person{name:"", city:"", age:0}
	p5.name = "七米"
	p5.age = 30
	p5.city = "成都"
	fmt.Printf("p3=%#v\n", p5) //p3=&main.person{name:"七米", city:"成都", age:30}
}

// 结构体初始化
func createStruct06() {
	type person struct {
		name string
		city string
		age  int8
	}
	//没有初始化的结构体，其成员变量对应其类型的零值
	var p6 person
	fmt.Printf("p4=%#v\n", p6) //p6=main.person{name:"", city:"", age:0}

	//使用键值对初始化
	p7 := person{
		name: "小王子",
		city: "北京",
		age:  18,
	}
	fmt.Printf("p5=%#v\n", p7) //p7=main.person{name:"小王子", city:"北京", age:18}

	//对结构体指针进行键值对初始化
	p8 := &person{
		name: "小王子",
		city: "北京",
		age:  18,
	}
	fmt.Printf("p6=%#v\n", p8) //p8=&main.person{name:"小王子", city:"北京", age:18}

	//使用值列表进行初始化
	p9 := &person{
		"沙河娜扎",
		"北京",
		28,
	}
	fmt.Printf("p8=%#v\n", p9) //p9=&main.person{name:"沙河那咋", city:"北京", age:19}
}

// 构造函数
func newPerson(name, city string, age int) *person {
	return &person{
		name: name,
		city: city,
		age:  age,
	}
}

// 方法和接收者 Person做梦的方法
func (p person) Dream() {
	fmt.Printf("%s的梦想是学好Go语言！\n", p.name)
}

// 指针类型接收者
func (p *person) SetAge(newAge int) {
	p.age = newAge
}

// 值类型接收者
func (p person) SetAge2(newAge int) {
	p.age = newAge
}

// 嵌套结构体
func createStruct07() {
	//Address 地址结构体
	type Address struct {
		Province string
		City     string
	}
	//User 用户结构体
	type User struct {
		Name    string
		Gender  string
		Address Address
	}

	user01 := User{
		Name:   "小王子",
		Gender: "男",
		Address: Address{
			Province: "山东",
			City:     "威海",
		},
	}
	fmt.Printf("user1=%#v\n", user01)
	// user1=main.User{Name:"小王子", Gender:"男", Address:main.Address{Province:"山东", City:"威海"}}
}

// 嵌套匿名字段
func createStruct08() {
	//Address 地址结构体
	type Address struct {
		Province string
		City     string
	}
	//User 用户结构体
	type User struct {
		Name    string
		Gender  string
		Address //匿名字段
	}

	var user2 User
	user2.Name = "小王子"
	user2.Gender = "男"
	user2.Address.Province = "山东" // 匿名字段默认使用类型名作为字段名
	user2.City = "威海"             // 匿名字段可以省略
	fmt.Printf("user2=%#v\n", user2)
	// //user2=main.User{Name:"小王子", Gender:"男", Address:main.Address{Province:"山东", City:"威海"}}
}

// 嵌套结构体的字段名冲突
func createStruct09() {
	//Address 地址结构体
	type Address struct {
		Province   string
		City       string
		CreateTime string
	}
	//Email 邮箱结构体
	type Email struct {
		Account    string
		CreateTime string
	}
	//User 用户结构体
	type User struct {
		Name   string
		Gender string
		Address
		Email
	}

	var user3 User
	user3.Name = "沙河娜扎"
	user3.Gender = "男"
	// user3.CreateTime = "2019" //ambiguous selector user3.CreateTime
	user3.Address.CreateTime = "2000" //指定Address结构体中的CreateTime
	user3.Email.CreateTime = "2000"   //指定Email结构体中的CreateTime
}

// 结构体JSON序列化
func createStruct10() {
	//Student 学生
	type Student struct {
		ID     int
		Gender string
		Name   string
	}
	//Class 班级
	type Class struct {
		Title    string
		Students []*Student
	}

	c := &Class{
		Title:    "101",
		Students: make([]*Student, 0, 200),
	}
	for i := 0; i < 10; i++ {
		stu := &Student{
			Name:   fmt.Sprintf("stu%02d", i),
			Gender: "男",
			ID:     i,
		}
		c.Students = append(c.Students, stu)
	}
	//JSON序列化：结构体-->JSON格式的字符串
	data, err := json.Marshal(c)
	if err != nil {
		fmt.Println("json marshal failed")
		return
	}
	fmt.Printf("json:%s\n", data)

	//JSON反序列化：JSON格式的字符串-->结构体
	str := `{"Title":"101","Students":[{"ID":0,"Gender":"男","Name":"stu00"},{"ID":1,"Gender":"男","Name":"stu01"},{"ID":2,"Gender":"男","Name":"stu02"},{"ID":3,"Gender":"男","Name":"stu03"},{"ID":4,"Gender":"男","Name":"stu04"},{"ID":5,"Gender":"男","Name":"stu05"},{"ID":6,"Gender":"男","Name":"stu06"},{"ID":7,"Gender":"男","Name":"stu07"},{"ID":8,"Gender":"男","Name":"stu08"},{"ID":9,"Gender":"男","Name":"stu09"}]}`
	c1 := &Class{}
	err = json.Unmarshal([]byte(str), c1)
	if err != nil {
		fmt.Println("json unmarshal failed!")
		return
	}
	fmt.Printf("%#v\n", c1)
}

// 结构体标签
func createStruct11() {
	//Student 学生
	type Student struct {
		ID     int    `json:"id"` //通过指定tag实现json序列化该字段时的key
		Gender string //json序列化是默认使用字段名作为key
		name   string //私有不能被json包访问
	}

	s1 := Student{
		ID:     1,
		Gender: "男",
		name:   "沙河娜扎",
	}
	data, err := json.Marshal(s1)
	if err != nil {
		fmt.Println("json marshal failed!")
		return
	}
	fmt.Printf("json str:%s\n", data) //json str:{"id":1,"Gender":"男"}
}
