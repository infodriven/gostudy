package main

import "gitlab.com/infodriven/gostudy/13golangBasePackage/utils"

func main() {
	utils.HelloGolang()
	utils.HelloWord()
	utils.HelloKali()
}
