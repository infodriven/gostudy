package utils

import "fmt"

func HelloWord() {
	fmt.Println("Hello Word")
}

func HelloGolang() {
	fmt.Println("Hello Golang")
}

func HelloKali() {
	fmt.Println("Hello Kali")
}
