package main

import (
	"fmt"
	"time"
)

func main() {
	//createChanel01()
	//createChannel02()
	//createChannel03()
	//ch := make(chan int)
	//go createChanel04(ch)  //启用goroutine从通道接收值
	//ch <- 10
	//fmt.Println("发送成功")
	//createChannel05()
	//	jobs := make(chan int, 100)
	//	results := make(chan int, 100)
	//	// 开启3个goroutine
	//	for w := 1; w <= 3; w++ {
	//		go worker(w, jobs, results)
	//	}
	//	// 5个任务
	//	for j := 1; j <= 5; j++ {
	//		jobs <- j
	//	}
	//	close(jobs)
	//	// 输出结果
	//	for a := 1; a <= 5; a++ {
	//		<-results
	//	}
	createChannel06()
}

func createChanel01() {
	//channel类型定义
	var ch1 chan int
	var ch2 chan bool
	var ch3 chan []int
	fmt.Printf("ch1:%T\nch2:%T\nch3:%T\n", ch1, ch2, ch3)
	fmt.Println(ch1, ch2, ch3) //通道是引用类型，通道类型的空值是nil。
}

func createChannel02() {
	ch1 := make(chan int)
	ch2 := make(chan bool)
	ch3 := make(chan []int)
	fmt.Printf("ch1:%T\nch2:%T\nch3:%T\n", ch1, ch2, ch3)
	fmt.Println(ch1, ch2, ch3) //通道是引用类型，默认值存储是地址
}

func createChannel03() {
	ch := make(chan int, 1)
	//将一个值发送到通道中
	ch <- 10
	//从一个通道中接收值
	x := <-ch //从通道中接收值并赋值给变量x
	close(ch)
	//我们通过调用内置的close函数来关闭通道
	fmt.Println("x=", x)
}

func createChanel04(c chan int) {
	ret := <-c
	fmt.Println("接收成功", ret)
}

func createChannel05() {
	ch1 := make(chan int)
	ch2 := make(chan int)
	// 开启goroutine将0~100的数发送到ch1中
	go func() {
		for i := 0; i < 100; i++ {
			ch1 <- i
		}
		close(ch1)
	}()
	// 开启goroutine从ch1中接收值，并将该值的平方发送到ch2中
	go func() {
		for {
			i, ok := <-ch1 // 通道关闭后再取值ok=false
			if !ok {
				break
			}
			ch2 <- i * i
		}
		close(ch2)
	}()
	// 在主goroutine中从ch2中接收值打印
	for i := range ch2 { // 通道关闭后会退出for range循环
		fmt.Println(i)
	}
}

func worker(id int, jobs <-chan int, results chan<- int) {
	for j := range jobs {
		fmt.Printf("worker:%d start job:%d\n", id, j)
		time.Sleep(time.Second)
		fmt.Printf("worker:%d end job:%d\n", id, j)
		results <- j * 2
	}
}

func createChannel06() {
	ch := make(chan int, 1)
	for i := 0; i < 10; i++ {
		select {
		case x := <-ch:
			fmt.Println(x)
		case ch <- i:
		}
	}
}
