package main

import "fmt"

func main() {
	createOperator01()
	createOperator02()
	createOperator03()
	createOperator04()
	createOperator05()
}

// Golang算数运算符
func createOperator01() {
	// 定义变量
	var a = 10
	var b = 2

	// 调试输出
	fmt.Println(a + b) // 相加
	fmt.Println(a - b) // 相减
	fmt.Println(a * b) // 相乘
	fmt.Println(a / b) // 相除
	fmt.Println(a % b) // 取余

	// 自加 & 自减
	a++
	b--
	fmt.Println(a) // 自加
	fmt.Println(b) // 自减
}

// Golang关系运算符
func createOperator02() {
	// 定义变量
	var a = 10
	var b = 2

	// 调试输出
	fmt.Println(a == b)
	fmt.Println(a != b)
	fmt.Println(a > b)
	fmt.Println(a < b)
	fmt.Println(a >= b)
	fmt.Println(a <= b)
}

// Golang逻辑运算符
func createOperator03() {
	a := 100
	b := 20
	fmt.Printf("变量a的数据类型是:%T,其对应的值是:%d\n", a, a)
	fmt.Printf("变量b的数据类型是:%T,其对应的值是:%d\n", b, b)

	// 逻辑非(!)，非真为假，非假为真
	fmt.Printf("a > b 结果为:[%t]\n", (a > b))
	fmt.Printf("a < b 结果为:[%t]\n", !(a > b))

	c := true
	d := false
	fmt.Printf("变量c的数据类型是:%T,其对应的值是:[%t]\n", c, c)
	fmt.Printf("变量c的数据类型是:%T,其对应的值是:[%t]\n", !c, !c)

	fmt.Printf("变量c的数据类型是:%T,其对应的值是:%t\n", c, c)
	fmt.Printf("变量d的数据类型是:%T,其对应的值是:%t\n", d, d)

	fmt.Printf("c && d 的结果为:[%t]\n", (c && d))
	fmt.Printf("c && d 的结果为:[%t]\n", (c || d))
}

// Golang位运算符
func createOperator04() {
	// 定义变量
	var a = 60 // 00111100
	var b = 13 // 00001101

	// 调试输出
	fmt.Println(a & b)  // 00001100: 12
	fmt.Println(a | b)  // 00111101: 61
	fmt.Println(a ^ b)  // 00110001: 49
	fmt.Println(a << 1) // 01111000: 120
	fmt.Println(a >> 1) // 00011110: 30
}

// Golang赋值运算符
func createOperator05() {
	// 定义变量
	var a = 10
	var b = 2
	var c int

	// 赋值运算符
	c = a
	fmt.Println(c)

	b += 2
	fmt.Println(b)

	b -= 2
	fmt.Println(b)

	a *= 2
	fmt.Println(a)

	a /= 2
	fmt.Println(a)

	a %= 3
	fmt.Println(a)
}
