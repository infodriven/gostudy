package main

import (
	"errors"
	"fmt"
)

func main() {
	createSayHello()
	ret := createSum(10, 20)
	fmt.Println(ret)

	ret1 := createSums()
	ret2 := createSums(10)
	ret3 := createSums(10, 20)
	ret4 := createSums(10, 20, 30)
	fmt.Println(ret1, ret2, ret3, ret4) //0 10 30 60

	// 将匿名函数保存到变量
	add := func(x, y int) {
		fmt.Println(x + y)
	}
	add(10, 20) // 通过变量调用匿名函数
	//自执行函数：匿名函数定义完加()直接执行
	func(x, y int) {
		fmt.Println(x + y)
	}(10, 20)
}

// 定义一个两数求和的函数
func createSum(x int, y int) int {
	return x + y
}

// 定义一个无返回值的函数
func createSayHello() {
	fmt.Println("Hello Golang")
}

// 定义一个可变参函数
func createSums(x ...int) int {
	fmt.Println(x) //x是一个切片
	sum := 0
	for _, v := range x {
		sum = sum + v
	}
	return sum
}

// 返回值函数
func calc(x, y int) (sum, sub int) {
	sum = x + y
	sub = x - y
	return
}

// 函数作为返回值
func do(s string) (func(int, int) int, error) {
	switch s {
	case "+":
		return add, nil
	case "-":
		return sub, nil
	default:
		err := errors.New("无法识别的操作符")
		return nil, err
	}
}

func sub(i int, i2 int) int {
	return i + i2
}

func add(i int, i2 int) int {
	return i + i2
}
