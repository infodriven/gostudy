package main

import "fmt"

func main() {
	createConst01()
	createConst02()
	createConst03()
	createConst04()
	createConst05()
	createConst06()
	createConst07()
}

func createConst01() {
	//声明常量
	const pi = 3.1415
	const e = 2.7182

	fmt.Println(pi, e)
}

func createConst02() {
	//声明多个常量
	const (
		n1 = 100
		n2
		n3
	)

	fmt.Println(n1, n2, n3)
}

func createConst03() {
	//iota
	const (
		n1 = iota //0
		n2        //1
		n3        //2
		n4        //3
	)

	fmt.Println(n1, n2, n3, n4)
}

func createConst04() {
	const (
		n1 = iota //0
		n2        //1
		_
		n4 //3
	)

	fmt.Println(n1, n2, n4)
}

func createConst05() {
	const (
		n1 = iota //0
		n2 = 100  //100
		n3 = iota //2
		n4        //3
	)
	const n5 = iota //0

	fmt.Println(n1, n2, n3, n4, n5)
}

func createConst06() {
	const (
		_  = iota
		KB = 1 << (10 * iota)
		MB = 1 << (10 * iota)
		GB = 1 << (10 * iota)
		TB = 1 << (10 * iota)
		PB = 1 << (10 * iota)
	)

	fmt.Println(KB, MB, GB, TB, PB)
}

func createConst07() {
	const (
		a, b = iota + 1, iota + 2 //1,2
		c, d                      //2,3
		e, f                      //3,4
	)

	fmt.Println(a, b, c, d, e, f)
}
