package main

import "fmt"

func main() {
	creatVariable01()
	createVariable02()
	createVariable03()
	createVariable04()
	createVariable05()
	x, _ := createVariable06()
	_, y := createVariable06()
	fmt.Printf("m=%d, x=%d y=%s", m, x, y)
}

// 1、标准声明
func creatVariable01() {
	// var 变量名 变量类型
	var name string
	var age int
	var isOk bool
	fmt.Println(name, age, isOk)
}

// 2、批量声明
func createVariable02() {
	var (
		a string
		b int
		c bool
		d float32
	)
	fmt.Println(a, b, c, d)
}

// 3、变量初始化
func createVariable03() {
	//var 变量名 类型 = 表达式
	var name string = "kali"
	var age int = 27
	fmt.Println(name, age)
}

// 4、类型推导
func createVariable04() {
	var name = "kali"
	var age = 27
	var addr, info = "Beijing", 66
	fmt.Println(name, age, addr, info)
}

// 5、段变量声明
// 全局变量m
var m = 100

func createVariable05() {
	n := 10
	m := 200
	fmt.Println(n, m, m)
}

// 6、匿名变量
func createVariable06() (int, string) {
	return 10, "kali"
}
